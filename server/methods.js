import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import fs from 'fs';
import carbone from 'carbone'
import { Tasks } from '../imports/api/tasks.js';
import { report } from 'process';

Meteor.methods({
    'addToRole' (role) {
        let countOfUsers = Meteor.users.find().count();
        if (typeof countOfUsers != 'undefined' && countOfUsers != null) {
            let userId = Meteor.userId();
            // if users = 1 - set to role passed in function call
            if (countOfUsers > 1) {
                console.log("User id for role: " + Meteor.userId() );
                Roles.addUsersToRoles(userId, "user");
            } else if (countOfUsers == 1) {
                console.log("Creating first system admin user: " + Meteor.userId() );            
                Roles.addUsersToRoles(userId, "system-admin");
            }
        } else {
            return;
        }
        
    },
    'report.outTime' (clientName, reportName, months, years) {
        check(clientName, String);
        check(reportName, String);
        check(months, [Number]);
        check(years, [Number]);
        
        let newData;

        console.log("months: ");
        console.dir(months);
        console.dir(years);

        if (Array.isArray(months)) {
            console.log("Is an empty array:");
        } else {
            console.log("    Not an Empty Array!");
        }

        // figure out the right query to run based on what values are sent....
        let reportFinal = '../../../../../public/' + reportName + '.pdf';

        if (!months.length && !years.length) {
            console.log("Running plain report - no months or yers sent.");
            newData = Tasks.find({ client: clientName, "taskTimes.endDateTime": { $ne: null }}).fetch();
        } else if (months.length > 0 && !years.length) {
            console.log("Running Month Inc report.");
            newData = Tasks.find({ client: clientName, "taskTimes.endDateTime": { $ne: null }, "taskTimes.Month": { $in: months }}).fetch();
        } else if (!months.length && years.length > 0) {
            console.log("Running Year Inc. Report.");
            newData = Tasks.find({ client: clientName, "taskTimes.endDateTime": { $ne: null }, "taskTimes.Year": { $in: years }}).fetch();
        } else {
            console.log("Running report with both month and year included.");
            newData = Tasks.find({ client: clientName, "taskTimes.endDateTime": { $ne: null }, "taskTimes.Month": { $in: months }, "taskTimes.Year": { $in: years }}).fetch();
        }

        let options = {
            convertTo : 'pdf'
        };

        console.dir(newData, { depth: null });

        let testFile = './npm/node_modules/carbone/examples/simple.odt';
        carbone.render(testFile, newData, options, function(err, result){
            if (err) {
            return console.log(err);
            } else {
                // write the result
                fs.writeFileSync(reportFinal, result);
                return "Should have a file now.";
            }
        });
    },
});