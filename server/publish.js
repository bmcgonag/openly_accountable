import { SysConfig } from '../imports/api/systemConfig.js';
import { UserConfigOptions } from '../imports/api/userConfigOptions.js';
import dayjs from 'dayjs';
import { Tasks } from '../imports/api/tasks.js';
import { TaskDates } from '../imports/api/taskDates.js';

Meteor.publish("SystemConfig", function() {
    try {
        return SysConfig.find({});
    } catch (error) {
        console.log("    ERROR pulling system config data: " + error);
    }
});

Meteor.publish("getTasks", function() {
    try {
        return Tasks.find({ taskBy: this.userId });
    } catch (error) {
        console.log("    ERROR: " + error);
    }
});

Meteor.publish("getAllTasks", function() {
    try {
        return Tasks.find({});
    } catch (error) {
        console.log("    ERROR: " + error);
    }
});

Meteor.publish('userList', function() { 
    return Meteor.users.find({});
});

Meteor.publish("getTaskDates", function() {
    try {
        return TaskDates.find({ taskDateBy: this.userId });
    } catch (error) {
        console.log("    ERROR: " + error);
    }
});
