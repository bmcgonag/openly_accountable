import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
  // code to run on server at startup
  Roles.createRole("system-admin", {unlessExists: true});
  Roles.createRole("admin", {unlessExists: true});
  Roles.createRole("user", {unlessExists: true});
  Roles.createRole("guest", {unlessExists: true});
});
