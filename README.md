# Openly_Accountable

by Brian McGonagill
Open Source Advocate
https://wiki.opensourceisawesome.com
https://youtube.com/c/AwesomeOpenSource

## Purpose
I felt like making a time tracker with a few extra options and abilities.  I also intend to setup some good reporting in it. I believe in being open and accountable when you are billing clients for time.

The application is structured in a way to allow you to start a time task, then edit the details when you have time, such as adding a client name to it, tags to categorize the type of task, notes for what work is done, and if enabbled by the system admin, mileage (start and finish). 

You can also do the following:
- Add notes to live tasks, or on tasks that have already ended
- Add / edit client names on live tasks, or on tasks that have already ended

To Do:
- Add / edit tags on live tasks, or on tasks that have already ended
- Add mileage to live tasks, or on tasks that have already ended
- End time tracking on a task, and continue it again later to catch time on tasks that may be broken up into several sessions.
- Setup Email to be sent out by the system to clients.
- Generate reports to be sent to clients.
- Setup dashboards to view time and tasks
- Create Employee groups (teams)
- View dashboards for a team (by a manager / team lead, etc)
- 

## Tech Stack
- MeteorJS using Blaxe templating
- MongoDB
- Materialize CSS / JS
- CarboneJS for reporting
- Docker (coming soon)

## Run for Development

Note: It's always good to create a new branch before making code chnages, then make a pull request which will then be merged with the main branch.

1. Install Meteor https://docs.meteor.com/install.html
2. Clone this repo `git clone https://gitlab.com/bmcgonag/openly_accountable.git`
3. Move into the cloned repo directory `cd openly_accountable`
4. install npm dependencies with `meteor npm install`
5. run the meteor application with `meteor`
6. Open the application in a browser at http://localhost:3000
7. Register and log in. 
8. Now make code changes, and when you save them, the meteor application will update in the browser automatically as long as you don't generate errors.

If you make an addition you'd like to share with us all, make a pull request back to the repo.

## Run as a User / Host
I'll eventually make this a dockerized container, and have a straight forward way to create a new instance. For now, however, you need to setup MongoDB, then I suggest using something like PM2 to run the node packages and keep them running.

1. Download the build from the Releases section.
2. The build is just a zipped archive of the build directory.
3.  More to come in way of specific commands.

## Authors and acknowledgment
Brian McGonagill
@bmcgonag on Github
@mickintx@fosstodon.org
https://wiki.opensourceisawesome.com
https://youtube.com/c/AwesomeOpenSource

## License
MIT Licensed

## Project status
Just starting this project.... We'll see how it goes.
