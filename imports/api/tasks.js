import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Tasks = new Mongo.Collection('tasks');

Tasks.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'add.taskAutoTime' (startDateTime) {
        check(startDateTime, Date);

        if (!this.userId) {
            throw new Meteor.Error('Not able to add Date / Time. Make sure you are logged in with valid credentials.');
        }

        let timeId = Math.floor(Math.random() * 100) * Date.now();
        let initialYear = new Date(startDateTime).getFullYear();
        let initialMonth = new Date(startDateTime).getMonth() + 1;
        let initialday = new Date(startDateTime).getDate();
        let initialdow = new Date(startDateTime).getDay();
        let initialHour = new Date(startDateTime).getHours();
        let initialMinute = new Date(startDateTime).getMinutes();
        let initialDate = initialdow + " " + initialMonth + "/" + initialday;
        
        let initialdayow;
        let initialshortday;
        switch (initialdow) {
            case 0:
                initialdayow = "Sunday";
                initialshortday = "Sun";
                break;
            case 1:
                initialdayow = "Monday";
                initialshortday = "Mon";
                break;
            case 2:
                initialdayow = "Tuesday";
                initialshortday = "Tue";
                break;
            case 3:
                initialdayow = "Wednesday";
                initialshortday = "Wed";
                break;
            case 4:
                initialdayow = "Thursday";
                initialshortday = "Thu";
                break;
            case 5:
                initialdayow = "Friday";
                initialshortday = "Fri";
                break;
            case 6:
                initialdayow = "Saturday";
                initialshortday = "Sat";
                break;
            default:
                initialdow = "Not Available";
                break;
        }

        return Tasks.insert({
            taskTimes: [{
                id: timeId,
                Month: initialMonth,
                Day: initialday,
                Year: initialYear,
                DayOfWeek: initialdayow,
                ShortDay: initialshortday,
                Hour: initialHour,
                Minute: initialMinute,
                startDateTime: startDateTime,
                endDateTime: null,
            }],
            client: null,
            notes: null,
            tags: null,
            mileage: [{}],
            taskBy: this.userId,
        });
    },
    'add.ManualTask' (startDateTime, endDateTime, clientName, notes, tags) {

    },
    'edit.taskNotes' (taskId, notes) {
        check(taskId, String);
        check(notes, String);

        if (!this.userId) {
            throw new Meteor.Error('Not able to add taskNotes Make sure you are logged in with valid credentials.');
        }

        return Tasks.update({ _id: taskId }, {
            $set: {
                notes: notes,
            }
        });
    },
    'edit.taskTags' (taskId, tags) {
        check(taskId, String);
        check(tags, [Object]);

        if (!this.userId) {
            throw new Meteor.Error('Not able to add / edit task tags. Make sure you are logged in with valid credentials.');
        }

        return Tasks.update({ _id: taskId }, {
            $set: {
                tags: tags,
            }
        });
    },
    'edit.taskClient' (taskId, client) {
        check(taskId, String);
        check(client, String);

        if (!this.userId) {
            throw new Meteor.Error('Not able to add taskNotes Make sure you are logged in with valid credentials.');
        }

        return Tasks.update({ _id: taskId }, {
            $set: {
                client: client,
            }
        });
    },
    'end.task' (taskId, endDateTime) {
        check(taskId, String);
        check(endDateTime, Date);

        if (!this.userId) {
            throw new Meteor.Error('Not able to end tasks. Make sure you are logged in with valid credentials.');
        }

        let timeId = Math.floor(Math.random() * 100) * Date.now();
        let endingYear = new Date(endDateTime).getFullYear();
        let endingMonth = new Date(endDateTime).getMonth() + 1;
        let endingday = new Date(endDateTime).getDate();
        let endingdow = new Date(endDateTime).getDay();
        let endingHour = new Date(endDateTime).getHours();
        let endingMinute = new Date(endDateTime).getMinutes();
        let endingDate = endingdow + " " + endingMonth + "/" + endingday;
        
        let endingdayow;
        let endingshortday;
        switch (endingdow) {
            case 0:
                endingdayow = "Sunday";
                endingshortday = "Sun";
                break;
            case 1:
                endingdayow = "Monday";
                endingshortday = "Mon";
                break;
            case 2:
                endingdayow = "Tuesday";
                endingshortday = "Tue";
                break;
            case 3:
                endingdayow = "Wednesday";
                endingshortday = "Wed";
                break;
            case 4:
                endingdayow = "Thursday";
                endingshortday = "Thu";
                break;
            case 5:
                endingdayow = "Friday";
                endingshortday = "Fri";
                break;
            case 6:
                endingdayow = "Saturday";
                endingshortday = "Sat";
                break;
            default:
                endingdow = "Not Available";
                break;
        }

        return Tasks.update({ _id: taskId, "taskTimes.endDateTime": null }, {
            $set: {
                "taskTimes.$.EndingMonth": endingMonth,
                "taskTimes.$.EndingDay": endingday,
                "taskTimes.$.EndingYear": endingYear,
                "taskTimes.$.EndingDayOfWeek": endingdayow,
                "taskTimes.$.EndingShortDay": endingshortday,
                "taskTimes.$.EndingHour": endingHour,
                "taskTimes.$.EndingMinute": endingMinute,
                "taskTimes.$.endDateTime": endDateTime,
            }
        });
    },
    'continue.task' (taskId, startDateTime) {
        check(taskId, String);
        check(startDateTime, Date);

        if (!this.userId) {
            throw new Meteor.Error('Not able to continue tasks. Make sure you are logged in with valid credentials.');
        }

        let timeId = Math.floor(Math.random() * 100) * Date.now();;
        let initialYear = new Date(startDateTime).getFullYear();
        let initialMonth = new Date(startDateTime).getMonth() + 1;
        let initialday = new Date(startDateTime).getDate();
        let initialdow = new Date(startDateTime).getDay();
        let initialHour = new Date(startDateTime).getHours();
        let initialMinute = new Date(startDateTime).getMinutes();
        let initialDate = initialdow + " " + initialMonth + "/" + initialday;
        
        let initialdayow;
        let initialshortday;
        switch (initialdow) {
            case 0:
                initialdayow = "Sunday";
                initialshortday = "Sun";
                break;
            case 1:
                initialdayow = "Monday";
                initialshortday = "Mon";
                break;
            case 2:
                initialdayow = "Tuesday";
                initialshortday = "Tue";
                break;
            case 3:
                initialdayow = "Wednesday";
                initialshortday = "Wed";
                break;
            case 4:
                initialdayow = "Thursday";
                initialshortday = "Thu";
                break;
            case 5:
                initialdayow = "Friday";
                initialshortday = "Fri";
                break;
            case 6:
                initialdayow = "Saturday";
                initialshortday = "Sat";
                break;
            default:
                initialdow = "Not Available";
                break;
        }
        return Tasks.update({ _id: taskId }, {
            $addToSet: {
                taskTimes: {
                    id: timeId,
                    Month: initialMonth,
                    Day: initialday,
                    Year: initialYear,
                    DayOfWeek: initialdayow,
                    ShortDay: initialshortday,
                    Hour: initialHour,
                    Minute: initialMinute,
                    startDateTime: startDateTime,
                    endDateTime: null
                }
            }
        });
    },
    'update.TaskClientNames' (origVal, newVal) {
        check(origVal, String);
        check(newVal, String);

        if (!this.userId) {
            throw new Meteor.Error('Not allowed to update task client names. Make sure you are logged in with valid credentials.');
        }

        return Tasks.update({ client: origVal }, {
            $set: {
                client: newVal,
            }
        }, { multi: true });
    },
    'update.TaskTags' (origVal, newVal) {
        check(origVal, String);
        check(newVal, String);

        if (!this.userId) {
            throw new Meteor.Error('Not allowed to update task client names. Make sure you are logged in with valid credentials.');
        }

        return Tasks.update({ 'tags.tag': origVal }, {
            $set: {
                'tags.$.tag': newVal,
            }
        }, { multi: true });
    },
    'tasks.addMileage' (taskId, startMiles, endMiles) {
        check(taskId, String);
        check(startMiles, Number);
        check(endMiles, Number);

        if (!this.userId) {
            throw new Meteor.Error('Not allowed to update task mileage. Make sure you are logged in with valid credentials.');
        }

        // Let's check our mileage entries...
        let mileTasks = Tasks.findOne({ _id: taskId, 'mileage.endMiles': 0 });
        
        if (typeof mileTasks != 'undefined') {
            if (endMiles != 0) {
                if (startMiles > endMiles) {
                    throw new Meteor.Error("Cannot have start miles greater than end miles.");
                } else {
                    let totalMiles = (endMiles - startMiles).toFixed(1);
                    return Tasks.update({ _id: taskId, 'mileage.endMiles': 0 }, {
                        $set: {
                            'mileage.$.endMiles': endMiles,
                            'mileage.$.totalMiles': totalMiles,
                        }
                    });
                }
            } 
        } else {
            return Tasks.update({ _id: taskId }, {
                $addToSet: {
                    mileage: {
                        startMiles: startMiles,
                        endMiles: endMiles,
                        totalMiles: 0,
                    }
                }
            });
        }
    },
});