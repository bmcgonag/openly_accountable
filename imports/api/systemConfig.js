import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const SysConfig = new Mongo.Collection('sysConfig');

SysConfig.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'add.typeInfo' (info, type) {
        check(info, [Object]);
        check(type, String);

        if (!this.userId) {
            throw new Meteor.Error('Not allowed to add tags to the settings. Make sure you are logged in with valid credentials.');
        }

        let noInfo = info.length;
        for (i=0; i<noInfo; i++) 
        {
            if (type == "tag") {
                SysConfig.insert({
                    info: info[i].tag,
                    type: type,
                });
            } else if (type == "client") {
                SysConfig.insert({
                    info: info[i].tag,
                    type: type,
                });
            }
        }
    },
    'add.reqMilage' (info, type) {
        check(info, Boolean);
        check(type, String);

        if (!this.userId) {
            throw new Meteor.Error('Not allowed to set milage require in the settings. Make sure you are logged in with valid credentials.');
        }

        // check if the info already exists, and if so, update instead of insert
        let reqMileExists = SysConfig.findOne({ type: "reqMilage" });
        if (typeof reqMileExists != 'undefined') {
            SysConfig.update({ _id: reqMileExists._id }, {
                $set: {
                    info: info,
                    type: type,
                }
            });
        } else {
            SysConfig.insert({
                info: info,
                type: type,
            });
        }

        if (info == true) {
            Meteor.call("add.allowMilage", true, "allowMilage");
        }
    },
    'add.allowMilage' (info, type) {
        check(info, Boolean);
        check(type, String);

        if (!this.userId) {
            throw new Meteor.Error('Not allowed to set milage allowed in the settings. Make sure you are logged in with valid credentials.');
        }

        let allowMileExists = SysConfig.findOne({ type: "allowMilage" });
        if (typeof allowMileExists != 'undefined') {
            SysConfig.update({ _id: allowMileExists._id }, {
                $set: {
                    info: info,
                    type: type,
                }
            });
        } else {
            SysConfig.insert({
                info: info,
                type: type,
            });
        }

        if (info == false){
            Meteor.call("add.reqMilage", false, "reqMilage");
        }
    },
    'add.allowContinue' (info, type) {
        check(info, Boolean);
        check(type, String);

        if (!this.userId) {
            throw new Meteor.Error('Not allowed to set task continuation preferences in the settings. Make sure you are logged in with valid credentials.');
        }
        let allowContExists = SysConfig.findOne({ type: "allowContinue" });
        if (typeof allowContExists != 'undefined') {
            SysConfig.update({ _id: allowContExists._id }, {
                $set: {
                    info: info,
                    type: type,
                }
            });
        } else {
            SysConfig.insert({
                info: info,
                type: type,
            });
        }
    },
    'add.autoUpdateDateTime' (info, type) {
        check(info, Boolean);
        check(type, String);

        if (!this.userId) {
            throw new Meteor.Error('Not allowed to set task autoupdate preferences in the settings. Make sure you are logged in with valid credentials.');
        }

        let allowContExists = SysConfig.findOne({ type: "autoUpdateDateTime" });
        if (typeof allowContExists != 'undefined') {
            SysConfig.update({ _id: allowContExists._id }, {
                $set: {
                    info: info,
                    type: type,
                }
            });
        } else {
            SysConfig.insert({
                info: info,
                type: type,
            });
        }
    },
    'delete.setting' (settingId) {
        check(settingId, String);

        if (!this.userId) {
            throw new Meteor.Error('Not allowed to delete settings. Make sure you are logged in with valid credentials.');
        }

        return SysConfig.remove({ _id: settingId });
    },
    'update.ClientSetting' (origVal, newVal) {
        check(origVal, String);
        check(newVal, String);

        if (!this.userId) {
            throw new Meteor.Error('Not allowed to update client name settings. Make sure you are logged in with valid credentials.');
        }

        return SysConfig.update({ type: 'client', info: origVal }, {
            $set: {
                info: newVal,
            }
        });
    },
    'update.tagSetting' (tagId, origVal, newVal) {
        check(tagId, String);
        check(origVal, String);
        check(newVal, String);

        if (!this.userId) {
            throw new Meteor.Error('Not allowed to update client name settings. Make sure you are logged in with valid credentials.');
        }

        return SysConfig.update({ _id: tagId }, {
            $set: {
                info: newVal,
            }
        });
    }
});