import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const TaskDates = new Mongo.Collection('taskDates');

TaskDates.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'add.newTaskDate' (taskDate, taskId) {
        check(taskDate, Date);
        check(taskId, String);

        if (!this.userId) {
            throw new Meteor.Error('Not able to add task dates. Make sure you are logged in with valid credentials.');
        }

        let taskYear = new Date(taskDate).getFullYear();
        let taskMonth = new Date(taskDate).getMonth() + 1;
        let taskDay = new Date(taskDate).getDate();

        // check to see if the date for the tasks already exists, and if so, update it instead.
        let currTasks = TaskDates.findOne({ taskYear: taskYear, taskMonth: taskMonth, taskDay: taskDay });

        if (typeof currTasks != 'undefined' && currTasks != null && currTasks != "") {
            let taskDateId = currTasks._id;
            Meteor.call("edit.taskDate", taskDateId, taskDate, taskId, function(err, result) {
                if (err) {
                    console.log("    ERROR editing Task Date info: " + err);
                } else {
                    console.log("Successfully updated task date data.");
                }
            });
        } else {
            return TaskDates.insert({
                taskDate: taskDate,
                taskYear: taskYear,
                taskMonth: taskMonth,
                taskDay: taskDay,
                taskDateBy: this.userId,
                daysTaskIds: [
                    {
                        taskId: taskId,
                    }
                ]
            });
        }
    },
    'edit.taskDate' (taskDateId, taskDate, taskId) {
        check(taskDateId, String);
        check(taskDate, Date);
        check(taskId, taskId);

        if (!this.userId) {
            throw new Meteor.Error('Not able to edit task date info. Make sure you are logged in with valid credentials.');
        }

        return TaskDates.update({ _id: taskDateId }, {
            $addToSet: {
                daysTaskIds: {
                    taskId: taskId 
                }
            }
        });
    }
});