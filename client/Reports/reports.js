import fs from 'fs';
import dayjs from 'dayjs';
import localizedFormat from 'dayjs/plugin/localizedFormat';
import { Tasks }from '../../imports/api/tasks.js';
import { SysConfig } from '../../imports/api/systemConfig.js';
import { timeStamp } from 'console';

Template.reports.onCreated(function() {
    this.subscribe("getTasks");
    this.subscribe("SystemConfig");
    dayjs.extend(localizedFormat);
});

Template.reports.onRendered(function() {
    $('select').formSelect();
    Meteor.setTimeout(function() {
        $('select').formSelect();
    },150)
});

Template.reports.helpers({
    clients: function() {
        return SysConfig.find({ type: "client" });
    },
});

Template.reports.events({
    'click #runReport1' (event) {
        let clientName = $("#clientNameReport").val();
        let month = $("#monthOfService").val();
        let year = $("#yearOfService").val();
        let reportName = $("#reportName").val();
        let monthNum = [];
        let yearNum = [];

        console.log("months: ");
        console.dir(month);
        
        if (clientName == null || clientName == "" || reportName == null || reportName == "") {
            showSnackbar("Missing Required Information!", "red");
            return;
        } else {
            if (month == "" || month == null) {
                monthNum = [];
            } else {
                monthNum = month.map(Number);
            }

            if (year == "" || year == null) {
                yearNum = [];
            } else {
                yearNum = year.map(Number);
            }

            Meteor.call('report.outTime', clientName, reportName, monthNum, yearNum, function(err, result) {
                if (err) {
                    console.log("    ERROR running report: " + err);
                    showSnackbar("Error Creating Report!", "red");
                } else {
                    console.log("Report should have run successfully.");
                    showSnackbar("Report Successfully Created!", "green");
                }
            });
        }
    }
});