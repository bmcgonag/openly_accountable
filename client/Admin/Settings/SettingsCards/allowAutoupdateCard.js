import { SysConfig } from '../../../../imports/api/systemConfig.js';

Template.allowAutoupdateCard.onCreated(function() {
    this.subscribe("SystemConfig");
});

Template.allowAutoupdateCard.onRendered(function() {
    $('.chips').chips();
    $('.tags-placeholder').chips({
        placeholder: 'Enter a tag',
        secondaryPlaceholder: '+Tag',
    });

    $('.clients-placeholder').chips({
        placeholder: 'Enter a tag',
        secondaryPlaceholder: '+Tag',
    });
});

Template.allowAutoupdateCard.helpers({
    'autoUpdateDT': function() {
        let upd = SysConfig.findOne({ type: 'autoUpdateDateTime' });
        if (typeof upd != 'undefined') {
            if (upd.info == true) {
                $("#autoUpdateDateTime").prop('checked', true);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    },
});

Template.allowAutoupdateCard.events({
    'change #autoUpdateDateTime' (event) {
        let upd = $("#autoUpdateDateTime").prop('checked');
        console.log("Auto Update Value Changed.");
        addAutoUpdate(upd, "autoUpdateDateTime");
    },
});

let addAutoUpdate = function(info, type) {
    Meteor.call('add.autoUpdateDateTime', info, type, function(err, result) {
        if (err) {
            console.log("    ERROR setting autoupdate of task date / time: " + err);
            showSnackbar("Error Setting Auto-Update Preference!", "red");
        } else {
            showSnackbar("Auto-Update Preference Set!", "green");
        }
    });
}
