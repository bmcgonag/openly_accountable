import { SysConfig } from '../../../../imports/api/systemConfig.js';

Template.mileageSettingsCard.onCreated(function() {
    this.subscribe("SystemConfig");
});

Template.mileageSettingsCard.onRendered(function() {
    $('.chips').chips();
    $('.tags-placeholder').chips({
        placeholder: 'Enter a tag',
        secondaryPlaceholder: '+Tag',
    });

    $('.clients-placeholder').chips({
        placeholder: 'Enter a tag',
        secondaryPlaceholder: '+Tag',
    });
});

Template.mileageSettingsCard.helpers({
    allowMiles: function() {
        let allow = SysConfig.findOne({ type: "allowMilage" });
        if (typeof allow != 'undefined') {
            if (allow.info == true) {
                $("#allowMilage").prop('checked', true);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    },
    reqMiles: function() {
        let req = SysConfig.findOne({ type: 'reqMilage' });
        if (typeof req != 'undefined') {
            if (req.info == true) {
                $("#reqMilage").prop('checked', true);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    },
});

Template.mileageSettingsCard.events({
    'change #reqMilage' (event) {
        let reqMile = $("#reqMilage").prop('checked');
        addMilages(reqMile, "reqMilage");
    },
    'change #allowMilage' (event) {
        let allowMile = $("#allowMilage").prop('checked');
        addMilages(allowMile, "allowMilage");
    },
});

let addMilages = function(info, type) {
    if (type == "reqMilage") {
        Meteor.call('add.reqMilage', info, type, function(err, result) {
            if (err) {
                console.log("    ERROR adding milage setting: " + err);
                showSnackbar("Error Setting Milage Preference!", "red");
            } else {
                showSnackbar("Milage Preference Set!", "green");
            }
        });
    } else {
        Meteor.call('add.allowMilage', info, type, function(err, result) {
            if (err) {
                console.log("    ERROR adding milage setting: " + err);
                showSnackbar("Error Setting Milage Preference!", "red");
            } else {
                showSnackbar("Milage Preference Set!", "green");
            }
        });
    }
}