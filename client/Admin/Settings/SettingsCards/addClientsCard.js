import { SysConfig } from '../../../../imports/api/systemConfig.js';

Template.addClientsCard.onCreated(function() {
    this.subscribe("SystemConfig");
});

Template.addClientsCard.onRendered(function() {
    $('.chips').chips();
    $('.tags-placeholder').chips({
        placeholder: 'Enter a tag',
        secondaryPlaceholder: '+Tag',
    });

    $('.clients-placeholder').chips({
        placeholder: 'Enter a tag',
        secondaryPlaceholder: '+Tag',
    });
});

Template.addClientsCard.helpers({
    clients: function() {
        return SysConfig.find({ type: 'client' });
    },
});

Template.addClientsCard.events({
    'click #saveClients' (event) {
        var clients = M.Chips.getInstance($('#clients')).chipsData;
        if (clients == null || clients == "") {
            showSnackbar("You must enter at least 1 client!", "orange");
            return;
        } else {
            Meteor.call('add.typeInfo', clients, "client", function(err, result) {
                if (err) {
                    console.log("    ERROR adding clients: " + err);
                    showSnackbar("Error Adding Clients!", "red");
                } else {
                    showSnackbar("Added Clients Successfully!", "green");
                }
            });
        } 
    },
    'click #cancelAddClients' (event) {

    },
    'click #editClients' (event) {
        FlowRouter.go('/editClientNames');
    },
});