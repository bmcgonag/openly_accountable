import { SysConfig } from '../../../../imports/api/systemConfig.js';

Template.allowContinueCard.onCreated(function() {
    this.subscribe("SystemConfig");
});

Template.allowContinueCard.onRendered(function() {
    $('.chips').chips();
    $('.tags-placeholder').chips({
        placeholder: 'Enter a tag',
        secondaryPlaceholder: '+Tag',
    });

    $('.clients-placeholder').chips({
        placeholder: 'Enter a tag',
        secondaryPlaceholder: '+Tag',
    });
});

Template.allowContinueCard.helpers({
    'allowCont': function() {
        let cont = SysConfig.findOne({ type: "allowContinue" });
        if (typeof cont != 'undefined') {
            if (cont.info == true) {
                $("#allowContinue").prop('checked', true);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    },
});

Template.allowContinueCard.events({
    'change #allowContinue' (event) {
        let allowContinue = $("#allowContinue").prop('checked');
        // we'll use the same function for mileages..we just will set ability to 
        // continue a task instead.
        addAllowContinue(allowContinue, "allowContinue");
    },
});

let addAllowContinue = function(info, type) {
    Meteor.call('add.allowContinue', info, type, function(err, result) {
        if (err) {
            console.log("    ERROR setting allow continuation of task: " + err);
            showSnackbar("Error Setting Task Continuation Preference!", "red");
        } else {
            showSnackbar("Task Continuation Preference Set!", "green");
        }
    });
}
