import { SysConfig } from '../../../../imports/api/systemConfig.js';

Template.addTagsCard.onCreated(function() {
    this.subscribe("SystemConfig");
});

Template.addTagsCard.onRendered(function() {
    $('.chips').chips();
    $('.tags-placeholder').chips({
        placeholder: 'Enter a tag',
        secondaryPlaceholder: '+Tag',
    });

    $('.clients-placeholder').chips({
        placeholder: 'Enter a tag',
        secondaryPlaceholder: '+Tag',
    });
});


Template.addTagsCard.helpers({
    tags: function() {
        return SysConfig.find({ type: 'tag' });
    },
});

Template.addTagsCard.events({
    'click #saveTags' (event) {
        var tags = M.Chips.getInstance($('#tags')).chipsData;
        if (tags == null || tags == "") {
            showSnackbar("You must enter at least 1 tag!", "orange");
            return;
        } else {
            Meteor.call('add.typeInfo', tags, "tag", function(err, result) {
                if (err) {
                    console.log("    ERROR adding tags: " + err);
                    showSnackbar("Error Adding Tags!", "red");
                } else {
                    showSnackbar("Added Tags Successfully!", "green");
                }
            });
        } 
    },
    'click #cancelAddTags' (event) {

    },
    'click #editTags' (event) {
        FlowRouter.go('/editTags');
    },
});