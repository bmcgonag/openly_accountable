import { SysConfig } from "../../../../imports/api/systemConfig.js";

Template.editClientNames.onCreated(function() {
    this.subscribe("SystemConfig");
});

Template.editClientNames.onRendered(function() {
    $('.modal').modal();
});

Template.editClientNames.helpers({
    clients: function() {
        return SysConfig.find({ type: "client" });
    },
});

Template.editClientNames.events({
    'click .editClientName' (event) {
        let clientId = this._id;
        Session.set("editModalTitle", "Client Name");
        Session.set("editModalId", clientId);
        Session.set("editModalVal", this.info);
        Session.set("updateMethod", "update.ClientSetting");
        $("#editModal").modal('open');
    },
    'click .deleteClientName' (event) {
        let clientId = this._id;
        Session.set("deleteId", clientId);
        Session.set("item", this.info);
        Session.set("method", "delete.setting");
        Meteor.setTimeout(function() {
            $("#modalDelete").modal('open');
        }, 100);
    },
});