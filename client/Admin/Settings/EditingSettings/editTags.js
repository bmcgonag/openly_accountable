import { SysConfig } from "../../../../imports/api/systemConfig.js";

Template.editTags.onCreated(function() {
    this.subscribe("SystemConfig");
});

Template.editTags.onRendered(function() {
    Session.set("editTagMode", false);
    $('.modal').modal();
});

Template.editTags.helpers({
    tags: function() {
        return SysConfig.find({ type: 'tag' });
    },
});

Template.editTags.events({
    'click .editTagName' (event) {
        let tagId = this._id;
        let tagVal = this.info;
        Session.set("editModalId", tagId);
        Session.set("editModalVal", tagVal);
        Session.set("editModalTitle", "Tag Name");
        Session.set("updateMethod", "update.tagSetting");
        $("#editModal").modal('open');
    },
    'click .deleteTagName' (event) {
        console.log("delete clicked.");
        let tagId = this._id;
        Session.set("deleteId", tagId);
        Session.set("item", this.info);
        Session.set("method", "delete.setting");
        Meteor.setTimeout(function() {
            $("#modalDelete").modal('open');
        }, 100);
    }
});
