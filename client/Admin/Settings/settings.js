import { SysConfig } from '../../../imports/api/systemConfig.js';

Template.settings.onCreated(function() {
    this.subscribe("SystemConfig");
});

Template.settings.onRendered(function() {
    $('.chips').chips();
    $('.tags-placeholder').chips({
        placeholder: 'Enter a tag',
        secondaryPlaceholder: '+Tag',
    });

    $('.clients-placeholder').chips({
        placeholder: 'Enter a tag',
        secondaryPlaceholder: '+Tag',
    });
});

Template.settings.helpers({
    
});

Template.settings.events({
    
});
