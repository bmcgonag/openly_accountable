

Template.userMgmt.onCreated(function() {
    this.subscribe("userList");
});

Template.userMgmt.onRendered(function() {
    $('select').formSelect();
});

Template.userMgmt.helpers({
    userInfo: function() {
        return Meteor.users.find({});
    },
    userName: function() {
        return this.profile.fullname;
    },
    userEmail: function() {
        return this.emails[0].address;
    },
    userRole: function() {
        return Roles.getRolesForUser( this._id );
    }
});

Template.userMgmt.events({
    "click .editUser" (event) {
        event.preventDefault();

        let userId = this._id;
        // take action
        console.log("Edit called on: " + userId);
    },
    "click .deleteUser" (event) {
        event.preventDefault();
        
        let userId = this._id;
        // take action
        console.log("Delete called on : " + userId);
    }
});