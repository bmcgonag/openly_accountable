Template.deleteConfirmationModal.onCreated(function() {

});

Template.deleteConfirmationModal.onRendered(function() {
    $('.modal').modal();
});

Template.deleteConfirmationModal.helpers({
    itemName: function() {
        return Session.get("item");
    },
});

Template.deleteConfirmationModal.events({
    'click .confirmDelete' (event) {
        event.preventDefault();
        let deleteId = Session.get("deleteId");
        let method = Session.get("method");

        Meteor.call(method, deleteId, function(err, result) {
            if (err) {
                console.log("    ERROR deleting item from modal: " + err);
            } else {
                // console.log("    SUCCESSFULLY deleted.");
                $('#modalDelete').modal('close');
            }
        });
    }, 
});