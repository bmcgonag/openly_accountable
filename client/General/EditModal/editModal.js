import { SysConfig } from '../../../imports/api/systemConfig.js';
import { Tasks } from '../../../imports/api/tasks.js';

Template.editModal.onCreated(function() {
    this.subscribe("SystemConfig");
    this.subscribe("getAllTakss");
});

Template.editModal.onRendered(function() {
    $('.modal').modal();
    $('.tooltipped').tooltip();
});

Template.editModal.helpers({
    modalTitle: function() {
        return Session.get("editModalTitle");
    },
    modalId: function() {
        return Session.get("editModalId");
    },
    modalVal: function() {
        return Session.get("editModalVal");
    },
});

Template.editModal.events({
    'click #updateModalEdit' (event) {
        let method = Session.get("updateMethod");
        let updateId = Session.get("editModalId");
        let origVal = Session.get("editModalVal");
        let newVal = $("#editModalField").val();
        let updateTasks = $("#updateTasks").prop('checked');
        console.log("New Tag Val = " + newVal);
        console.log("Orig Tag Val = " + origVal);
        if (origVal != newVal) {
            console.log("New Value good.");
            if (updateTasks == true) {
                if (method == "update.ClientSetting") {
                    Meteor.call('update.TaskClientNames', origVal, newVal, function(err, result) {
                        if (err) {
                            console.log("    ERROR updating task client names: " + err);
                            showSnackbar("Error Updating Existing Task Client Names.", "red");
                        } else {
                            updateClientSetting(origVal, newVal);
                        }
                    });
                } else if (method == "update.tagSetting") {
                    Meteor.call('update.TaskTags', origVal, newVal, function(err, result) {
                        if (err) {
                            console.log("    ERROR updating task tag: " + err);
                            showSnackbar("Error Updating Task Tags!", "red");
                        } else {
                            updateTagSetting(updateId, origVal, newVal);
                        }
                    });
                }
            } else {
                if (method == "update.ClientSetting") {
                    updateClientSetting(origVal, newVal);
                } else if (method == "update.tagSetting") {
                    console.log("straight to tag setting update.");
                    updateTagSetting(updateId, origVal, newVal);
                }
            }
        } else {
            showSnackbar("You must enter a new value!", "orange");
            return;
        }
    }
});

let updateClientSetting = function(origVal, newVal) {
    Meteor.call("update.ClientSetting", origVal, newVal, function(err, result) {
        if (err) {
            console.log("    ERROR updating the Client Setting: " + err);
            showSnackbar("Error Updating the Client Name Setting!", "red");
        } else {
            showSnackbar("Client Entries Updated Successfully!", "green");
            $("#editModal").modal('close');
        }
    });
}

let updateTagSetting = function(tagId, origVal, newVal) {
    Meteor.call('update.tagSetting', tagId, origVal, newVal, function(err, result) {
        if (err) {
            console.log("    ERROR updating the tag name in settings: " + err);
            showSnackbar("Error Updating Tag Name Setting!", "red");
        } else {
            showSnackbar("Tag Name Setting Updated Successfully!", "green");
            $("#editModal").modal('close');
        }
    });
}