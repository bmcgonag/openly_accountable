import { Tasks } from '../../../../imports/api/tasks.js';
import { SysConfig } from '../../../../imports/api/systemConfig.js';

Template.adjustTimeModal.onCreated(function() {
    this.subscribe("getTasks");
    this.subscribe("SystemConfig");
});

Template.adjustTimeModal.onRendered(function() {
    $('.datepicker').datepicker();
    $('.timepicker').timepicker();
    Meteor.setTimeout(function() {
        $('.datepicker').datepicker();
        $('.timepicker').timepicker();
    }, 150);
});

Template.adjustTimeModal.helpers({
    myTask: function() {
        return Tasks.findOne({ 'taskTimes.id': Session.get("editTimeId") });
    },
    startDate: function() {
        return new Date(this.startDateTime).toLocaleDateString('en-us', { year:"numeric", month:"short", day:"numeric"});
    },
    endDate: function() {
        return new Date(this.endDateTime).toLocaleDateString('en-us', { year:"numeric", month:"short", day:"numeric"});
    },
    startTime: function() {
        return new Date(this.startDateTime).toLocaleTimeString('en-us');
    },
    endTime: function() {
        return new Date(this.endDateTime).toLocaleTimeString('en-us');
    }
});

Template.adjustTimeModal.events({
    'change .startTime, change .startDate, change .endTime, change .endDate' (event) {
        let myConfig = SysConfig.findOne({ type: "autoUpdateDateTime" });
        if (typeof myConfig == undefined) {
            myConfig.info = false;
        }

        let evtOn = event.currentTarget.id;
        let mySelArray = evtOn.split('_');
        let id = mySelArray[1];
        let mySel = mySelArray[0];
        if (mySel == "startTime" || mySel == "startDate") {
            let newTime = $("#startTime_" + this.id).val();
            let endingTime = $("#endTime_" + this.id).val();
            let newDate = $("#startDate_" + this.id).val();
            let endingDate = $("#endDate_" + this.id).val();
            let newDate1 = Date.parse(newDate + " " + newTime);
            let endingDate1 = Date.parse(endingDate + " " + endingTime);
            let newDateTime = new Date(newDate + " " + newTime).toISOString(); // this is the value for the db
            if (myConfig.info == true) {
                if (newDate1 > endingDate1) {
                    $("#endTime_" + this.id).val(newTime);
                    $("#endDate_" + this.id).val(newDate);
                    console.log("New Time should be: " + newDateTime);
                    // write the new values to the db
    
                } else {
                    // just write the new values for startTime / Date
    
                }
            } else {
                // just write the new time and date, leave the other alone.

            }
            
        } else if (mySel == "endTime" || mySel == "endDate") {
            let newTime = $("#endTime_" + this.id).val();
            let startingTime = $("#startTime_" + this.id).val();
            let newDate = $("#endDate_" + this.id).val();
            let startingDate = $("#startDate_" + this.id).val();
            let newDate1 = Date.parse(newDate + " " + newTime);
            let startingDate1 = Date.parse(startingDate + " " + startingTime);
            let newDateTime = new Date(newDate + " " + newTime).toISOString();
            if (myConfig.info == true) {
                if (newDate1 < startingDate1) {
                    $("#startTime_" + this.id).val(newTime);
                    $("#startDate_" + this.id).val(newDate);      
                    console.log("New Time should be: " + newDateTime); // this is written to db
                    // write the new values to the db
    
                } else {
                    // just write the new values for end time / date
    
                }
            } else {
                // just write the new end time and date, leave the other alone.

            }
        }
    },
    'click #saveNewDateTime' (event) {

    },
});

