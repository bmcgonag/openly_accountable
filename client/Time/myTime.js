import { Tasks } from '../../imports/api/tasks.js';
import { SysConfig } from '../../imports/api/systemConfig.js';
import { TaskDates } from '../../imports/api/taskDates.js';

Template.myTime.onCreated(function() {
    this.subscribe("getTasks");
    this.subscribe("SystemConfig");
    this.subscribe('getTaskDates');
});

Template.myTime.onRendered(function() {
    Session.set("currentTime", false);
    Session.set("addEditNotes", false);
    Session.set("addEditTags", false);
    Session.set("addEditClient", false);
    Session.set("editHistNotes", false);
    Session.set("enterMileage", false);
    $('.collapsible').collapsible();
    $('select').formSelect();
    $('.chips').chips();
    $('.tags-placeholder').chips({
        placeholder: 'Enter a tag',
        secondaryPlaceholder: '+Tag',
    });
    this.autorun(function() {
        let tags = SysConfig.find({ type: "tag" }).fetch();
        let finalTags = {};
        let tagLength = tags.length;
        for (i=0; i<tagLength; i++) {
            finalTags[tags[i].info] = null;
        }
        Session.set("myTags", finalTags);
    });
    $(".modal").modal();
    Meteor.setTimeout(function() {
        $('.collapsible').collapsible();
        $('select').formSelect();
    }, 150);
});

Template.myTime.helpers({
    currCard: function() {
        let liveTasks = Tasks.find({ 'taskTimes.endDateTime': null }).count();
        if (liveTasks == 1) {            
            return true;
        } else {
            return false;
        }
    },
    taskDates: function() {
        return TaskDates.find({});
    },
    histLen: function() {
        let thismonth = this.taskMonth;
        let thisday = this.taskDay;
        let thisyear = this.taskYear;
        return Tasks.find({ 'taskTimes.Month': thismonth, 'taskTimes.Day': thisday, 'taskTimes.Year': thisyear, 'taskTimes.endDateTime': { $ne: null }});
    },
    taskMonthName: function() {
        switch(this.taskMonth) {
            case 1:
                return "Jan";
                break;
            case 2:
                return "Feb";
                break;
            case 3:
                return "Mar";
                break;
            case 4:
                return "Apr";
                break;
            case 5:
                return "May";
                break;
            case 6:
                return "June";
                break;
            case 7:
                return "July";
                break;
            case 8:
                return "Aug";
                break;
            case 9:
                return "Sep";
                break;
            case 10:
                return "Oct";
                break;
            case 11:
                return "Nov";
                break;
            case 12:
                return "Dec";
                break;
            default:
                break;
        }
    }
});

Template.currentTime.helpers({
    taskStarted: function() {
        return Tasks.findOne({ 'taskTimes.endDateTime': null });
    },
    startedTime: function() {
        let startedDate = (this.startDateTime).toLocaleDateString('en-us', { weekday:"short", year:"numeric", month:"short", day:"numeric"}); 
        let startedTime = (this.startDateTime).toLocaleTimeString('en-us');
        return (startedDate + " " + startedTime);
    },
    endedTime: function() {
        let endDT;
        let endedDate = (this.endDateTime).toLocaleDateString('en-us', { weekday:"short", year:"numeric", month:"short", day:"numeric"}); 
        let endedTime = (this.endDateTime).toLocaleTimeString('en-us');
        endDT = (endedDate + " " + endedTime);
        return endDT;
    },
    liveTags: function() {
        return this.tags;
    },
    liveClient: function() {
        return this.client;
    },
    liveNotes: function() {
        console.log("-----------------------------");
        console.log(this.notes);
        return this.notes;
    },
    liveStartMiles: function() {
        let taskInfoLive = Tasks.findOne({ 'taskTimes.endDateTime': null, 'mileage.endMiles': 0 });
        if (typeof taskInfoLive != 'undefined') {
            let infoLen = taskInfoLive.mileage.length;
            // console.log(taskInfoLive.mileage[infoLen-1].startMiles);
            return taskInfoLive.mileage[infoLen-1].startMiles;
        }
    },
    addEditNotes: function() {
        return Session.get("addEditNotes");
    },
    addEditTags: function() {
        return Session.get("addEditTags");
    },
    addEditClient: function() {
        return Session.get("addEditClient");
    },
    allowMile: function() {
        let mileAllow = SysConfig.findOne({ type: "allowMilage" });
        if (typeof mileAllow != 'undefined') {
            return mileAllow.info;
        }
    },
    reqMile: function() {
        return SysConfig.findOne({ type: "reqMilage" }).info;
    },
    clients: function() {
        return SysConfig.find({ type: 'client' });
    },
    enterMileage: function() {
        return Session.get("enterMileage");
    },
});

Template.myTime.events({
    'change #histLength' (event) {
        let histLen = $("#histLength").val();
        Session.set("histLength", histLen);
    },
    'click #startMyTime' (event) {
        event.preventDefault();
        Session.set("currentTime", true);
        let currDateTime = new Date();
        console.log("Curr date Time: " + currDateTime);
        Meteor.call('add.taskAutoTime', currDateTime, function(err, result) {
            if (err) {
                console.log("    ERROR starting timer! " + err);
                showSnackbar("Error Starting Timer!", "red");
            } else {
                showSnackbar("Timer Started!", "green");
                Meteor.call('add.newTaskDate', currDateTime, result, function(err, result) {
                    if (err) {
                        console.log("    ERROR adding new task date: " + err);
                    } else {
                        console.log("Successfully added new task date.");
                    }
                });
            }
        });
    },
    'click #endMyTime' (event) {
        event.preventDefault();
        let liveTask = Tasks.findOne({ 'taskTimes.endDateTime': null });
        let taskId = liveTask._id;
        Session.set("currentTime", false);
        let endCurrDateTime = new Date();
        Meteor.call("end.task", taskId, endCurrDateTime, function(err, result) {
            if (err) {
                console.log("    ERROR unable to end task: " + err);
                showSnackbar("Error Ending Task!", "red");
            } else {
                showSnackbar("Task Ended Successfully!", "green");
            }
        });
    },
    'click #liveNotes' (event) {
        let liveTask = Tasks.findOne({ "taskTimes.endDateTime": null });
        let taskId = liveTask._id;
        Session.set("liveTaskId", taskId);
        console.log("Notes clicked for taskId: " + taskId);
        Session.set("addEditNotes", true);
    },
    'click #saveLiveNotes' (event) {
        let taskId = Session.get("liveTaskId");
        console.log("taskId is: " + taskId);
        let noteText = $("#addEditNoteInfo").val();
        console.log(noteText);
        if (noteText != "" && noteText != null) {
            updateNotes(taskId, noteText);
        }
    },
    'click #liveTags' (event) {
        let liveTask = Tasks.findOne({ "taskTimes.endDateTime": null });
        let taskId = liveTask._id;
        Session.set("liveTaskId", taskId);
        Session.set("addEditTags", true);
        Meteor.setTimeout(function() {
            let myTags = Session.get("myTags");
            $('.chips').chips();
            $('.tags-placeholder').chips({
                placeholder: 'Enter a tag',
                secondaryPlaceholder: '+Tag',
            });
            $('.tags-autocomplete').chips({
                autocompleteOptions: {
                    data: myTags,
                }
            });
        },150);
    },
    'click #liveClient' (event) {
        let liveTask = Tasks.findOne({ "taskTimes.endDateTime": null });
        let taskId = liveTask._id;
        Session.set("liveTaskId", taskId);
        Session.set("addEditClient", true);
        Meteor.setTimeout(function() {
            $('select').formSelect();
        }, 150);
    },
    'click #saveLiveClient' (event) {
        let client = $("#clientName").val();
        let taskId = Session.get("liveTaskId");
        if (client != null && client != "") {
            updateClient(taskId, client);
        }
    },
    'click #saveLiveTags' (event) {
        let taskId = Session.get("liveTaskId");
        let taskTags = M.Chips.getInstance($('#taskTags')).chipsData;
        if (typeof taskTags != 'undefined' && taskTags != null && taskTags != "") {
            updateTaskTags(taskId, taskTags);
        } else {
            Session.set("addEditTags", false);
        }
    },
});

Template.currentTime.events({
    'click .removeLiveChip' (event) {
        console.dir(event);
    },
    'click #liveMileage' (event) {
        let liveTask = Tasks.findOne({ "taskTimes.endDateTime": null });
        let taskId = liveTask._id;
        Session.set("liveTaskId", taskId);
        Session.set("enterMileage", true);
    },
    'click #saveLiveMileage' (event) {
        let taskId = Session.get("liveTaskId");
        let startMilesVal = $("#startingMileage").val();
        let endMilesVal = $("#endingMileage").val();
        let startMiles = Number(startMilesVal);
        let endMiles = Number(endMilesVal);

        if (startMiles == null || startMiles == "") {
            showSnackbar("Starting Mileage is Required!", "orange");
            return;
        } else {
            if (endMiles > startMiles || endMiles == 0) {
                Meteor.call("tasks.addMileage", taskId, startMiles, endMiles, function(err, result) {
                    if (err) {
                        console.log("    ERROR adding mileage to live task: " + err);
                        showSnackbar("Error Adding Mileage to Live Task.", "red");
                    } else {
                        Session.set("enterMileage", false);
                        showSnackbar("Succesfully Added Mileage to Live Task!", "green");
                    }
                });
            } else {
                showSnackbar("Ending Cannot be Less Than Starting Mileage!", "red");
                return;
            }
        }
    }
});

Template.historicalTime.helpers({
    // histLen: function() {
    //     let histLen = Session.get("histLength");
    //     let now = new Date();
    //     let day = new Date(now.setHours(0,0,0,0));
    //     let today = day.getDay();
    //     let dateToday = day.getDate();
    //     let fullYear = day.getFullYear();
    //     let month = day.getMonth();

    //     let yesterday = new Date(day.setDate(day.getDate() - 1));
    //     let fiveday = new Date(day.setDate(day.getDate() - 5));
    //     let sevenday = new Date(day.setDate(day.getDate() - 7));
    //     let twoweeks = new Date(day.setDate(day.getDate() - 14));
    //     let onemonth = new Date(day.setDate(day.getDate() - 30));
    //     let onemonthfeb = new Date(day.setDate(day.getDate() - 28));
        
    //     // get 1st and last day of this week.
    //     let first = dateToday - today;
    //     let last = first + 6;
    //     let firstday = new Date(now.setDate(dateToday - today));
    //     let lastday = new Date(now.setDate((dateToday - today) + 6));

    //     // get fist and last day of current month
    //     let firstDayMon = new Date(fullYear, month, 1);
    //     let lastDayMon = new Date(fullYear, month + 1, 0);

    //     return Tasks.find({ _id: this.taskId });
    //     // switch (histLen) {
    //     //     case "1d":
    //     //         return Tasks.find({ 'taskTimes.endDateTime': { $ne: null }, 'taskTimes.startDateTime': { $gte: yesterday }});
    //     //         break;
    //     //     case "5d":
    //     //         return Tasks.find({ 'taskTimes.endDateTime': { $ne: null }, 'taskTimes.startDateTime': { $gte: fiveday }});
    //     //         break;
    //     //     case "1w":
    //     //         return Tasks.find({ 'taskTimes.endDateTime': { $ne: null }, 'taskTimes.startDateTime': { $gte: sevenday }});
    //     //         break;
    //     //     case "2w":
    //     //         return Tasks.find({ 'taskTimes.endDateTime': { $ne: null }, 'taskTimes.startDateTime': { $gte: twoweeks }});
    //     //         break;
    //     //     case "tw":
    //     //         return Tasks.find({ 'taskTimes.endDateTime': { $ne: null }, $and: [ { 'taskTimes.startDateTime': { $gte: firstday }}, { 'taskTimes.startDateTime': { $lte: lastday }} ] });
    //     //         break;
    //     //     case "1m":
    //     //         if (month == 1) {
    //     //             return Tasks.find({ 'taskTimes.endDateTime': { $ne: null }, 'taskTimes.startDateTime': { $gte: onemonthfeb }});
    //     //         } else {
    //     //             return Tasks.find({ 'taskTimes.endDateTime': { $ne: null }, 'taskTimes.startDateTime': { $gte: onemonth }});
    //     //         }
    //     //         break;
    //     //     case "tm":
    //     //         return Tasks.find({ 'taskTimes.endDateTime': { $ne: null }, $and: [ { 'taskTimes.startDateTime': { $gte: firstDayMon }}, { 'taskTimes.startDateTime': { $lte: lastDayMon }} ] });
    //     //         break;
    //     //     default:
    //     //         return Tasks.find({ 'taskTimes.endDateTime': { $ne: null }, 'taskTimes.startDateTime': { $gte: sevenday }});
    //     //         break;
    //     // }

    // },
    histCard: function() {
        return Tasks.find({ 'taskTimes.endDateTime': { $ne: null }});
    },
    startedTime: function() {
        let startDate = (this.startDateTime).toLocaleDateString('en-us', { weekday:"short", year:"numeric", month:"short", day:"numeric" });
        let startTime = (this.startDateTime).toLocaleTimeString('en-us');
        return (startDate + " " + startTime);
    },
    endedTime: function() {
        let endDate = (this.endDateTime).toLocaleDateString('en-us', { weekday:"short", year:"numeric", month:"short", day:"numeric" });
        let endTime = (this.endDateTime).toLocaleTimeString('en-us');
        return (endDate + " " + endTime);
    },
    totalTime: function() {
        let date_diff_min = Math.ceil(Math.abs(this.startDateTime - this.endDateTime)/60000)
        let date_diff_hr = Math.floor(date_diff_min/60);
        let date_diff_min_remain = date_diff_min%60;
        return (date_diff_hr + " hr " + date_diff_min_remain + " min");
    },
    histTaskId: function() {
        return this._id;
    },
    histTags: function() {
        return this.tags;
    },
    liveClient: function() {
        return this.clientName;
    },
    liveNotes: function() {
        console.log("-----------------------------");
        console.log(this.notes);
        return this.notes;
    },
    addEditHistNotes: function() {
        return Session.get("editHistNotes");
    },
    editHistClient: function() {
        return Session.get("editHistClient");
    },
    editHistTags: function() {
        return Session.get("editHistTags");
    },
    allowMile: function() {
        let mileAllow = SysConfig.findOne({ type: "allowMilage" });
        if (typeof mileAllow != 'undefined') {
            return mileAllow.info;
        }
    },
    reqMile: function() {
        return SysConfig.findOne({ type: "reqMilage" }).info;
    },
    allowCont: function() {
        let allowCont = SysConfig.findOne({ type: "allowContinue" });
        if (typeof allowCont != 'undefined') {
            // console.log("found a continue value." + allowCont.info);
            return allowCont.info;
        }
    },
    clients: function() {
        return SysConfig.find({ type: 'client' });
    },
});

Template.historicalTime.events({
    'click .histNotes' (event) {
        let taskId = this._id;
        Session.set("taskNoteHistId", taskId);
        console.log("Historical taask notes clicked for id: " + taskId);
        Session.set("editHistNotes", true);
    },
    'click .saveHistNotes' (event) {
        let taskId = Session.get("taskNoteHistId");
        console.log("taskId is: " + taskId);
        let noteText = $("#" + taskId).val();
        console.log(noteText);
        if (noteText != "" && noteText != null) {
            updateNotes(taskId, noteText);
        }
    },
    'click .histClient' (event) {
        let taskId = this._id;
        Session.set("taskClientHistId", taskId);
        Session.set("editHistClient", true);
        Meteor.setTimeout(function() {
            $('select').formSelect();
        }, 150);
    },
    'click .saveHistClient' (event) {
        let taskId = Session.get("taskClientHistId");
        let histClient = $("#" + taskId).val();
        if (histClient != "" && histClient != null) {
            updateClient(taskId, histClient);
        }
    },
    'click .histTags' (event) {
        let taskId = this._id;
        Session.set("tasksClientHistId", taskId);
        Session.set("editHistTags", true);
        Meteor.setTimeout(function() {
            let myTags = Session.get("myTags");
            $('.chips').chips();
            $('.tags-placeholder').chips({
                placeholder: 'Enter a tag',
                secondaryPlaceholder: '+Tag',
            });
            $('.tags-autocomplete').chips({
                autocompleteOptions: {
                    data: myTags,
                }
            });
        }, 150);
    },
    'click .saveHistTags' (event) {
        let taskId = this._id;
        let taskTags = M.Chips.getInstance($('#' + taskId)).chipsData;
        if (typeof taskTags != 'undefined' && taskTags != null && taskTags != "") {
            updateTaskTags(taskId, taskTags);
        } else {
            Session.set("editHistTags", false);
        }
    },
    'click .continueTime' (event) {
        let taskId = this._id;
        let rightNow = new Date();
        Meteor.call('continue.task', taskId, rightNow, function(err, result) {
            if (err) {
                console.log("    ERROR adding new start time: " + err);
                showSnackbar("Error Continuing Task!", "red");
            } else {
                showSnackbar("Task Timer Continued!", "green");
            }
        });
    },
    'click .historicalTimeListing' (event) {
        console.log("Time Id clicked: " + this.id);
        Session.set("editTimeId", this.id);
    },
    'click .removeTag' (event) {
        console.log("remove tag clicked.");
        let tagInfo = event.currentTarget.id;
        console.log("Tag to remove is: " + tagInfo);
        // Meteor.call("remove.TagFromTask", this._id, tagInfo, function(err, result) {
        //     if (err) {
        //         console.log("    ERROR tag could not be removed: " + err);
        //         showSnackbar("Error Removing Tag from Task!", "red");
        //     } else {
        //         showSnackbar("Tag Removed!", "green");
        //     }
        // });
    },
});


let updateNotes = function(taskId, noteText) {
    Meteor.call("edit.taskNotes", taskId, noteText, function(err, result) {
        if (err) {
            showSnackbar("Error Adding Notes!", "red");
            console.log("    ERROR adding notes! " + err);
        } else {
            showSnackbar("Notes Added to Task!", "green");
            Session.set("addEditNotes", false);
            Session.set("editHistNotes", false);
        }
    });
}

let updateClient = function(taskId, client) {
    Meteor.call('edit.taskClient', taskId, client, function(err, result) {
        if (err) {
            console.log("    ERROR adding / editing client to live task: " + err);
            showSnackbar("Error Changing Client!", "red");
        } else {
            Session.set("addEditClient", false);
            Session.set("editHistClient", false);
            showSnackbar("Client Successfully Updated!", "green");
        }
    });
}

let updateTaskTags = function(taskId, tags) {
    Meteor.call('edit.taskTags', taskId, tags, function(err, result) {
        if (err) {
            console.log("    ERROR adding tags to task: " + err);
            showSnackbar("Error Adding Tags!", "red");
        } else {
            Session.set("addEditTags", false);
            Session.set("editHistTags", false);
            showSnackbar("Tags Successfully Added!", "green");
        }
    });
}