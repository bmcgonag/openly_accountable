FlowRouter.route('/dashboard', {
    name: 'home',
    action() {
        BlazeLayout.render('MainLayout', { main: "dashboard" });
    }
});

FlowRouter.route('/', {
    name: 'myTime',
    action() {
        BlazeLayout.render('MainLayout', { main: "myTime" });
    }
});

FlowRouter.route('/', {
    name: 'homeNotLoggedIn',
    action() {
        BlazeLayout.render('MainLayout', { notLoggedIn: "login" });
    }
});

FlowRouter.route('/login', {
    name: 'login',
    action() {
        BlazeLayout.render('MainLayout', { notLoggedIn: "login" });
    }
});

FlowRouter.route('/reg', {
    name: 'reg',
    action() {
        BlazeLayout.render('MainLayout', { notLoggedIn: "reg" });
    }
});

FlowRouter.route('/userMgmt', {
    name: 'userMgmt',
    action() {
        BlazeLayout.render('MainLayout', { main: 'userMgmt' });
    }
});

FlowRouter.route('/myTime', {
    name: 'myTime',
    action() {
        BlazeLayout.render('MainLayout', { main: 'myTime' });
    }
});

FlowRouter.route('/settings', {
    name: 'settings',
    action() {
        BlazeLayout.render('MainLayout', { main: 'settings' });
    }
});

FlowRouter.route('/reports', {
    name: 'reports',
    action() {
        BlazeLayout.render('MainLayout', { main: 'reports' });
    }
});

FlowRouter.route('/editTags', {
    name: 'editTags',
    action() {
        BlazeLayout.render('MainLayout', { main: 'editTags' });
    }
});

FlowRouter.route('/editClientNames', {
    name: 'editClientNames',
    action() {
        BlazeLayout.render('MainLayout', { main: 'editClientNames' });
    }
});